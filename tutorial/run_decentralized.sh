#!/bin/bash

decpy_path=../eval
cd $decpy_path

env_python=python3
graph=./35_workers.edges
original_config=../tutorial/config_celeba_sharing.ini
config_file=config.ini
procs_per_machine=35
machines=1
iterations=2000
test_after=50
eval_file=testing.py
log_level=INFO

m=0
echo M is $m
log_dir=$(date '+%Y-%m-%dT%H:%M')/machine$m
mkdir -p $log_dir

cp $original_config $config_file
# echo "alpha = 0.10" >> $config_file
$env_python $eval_file -ro 0 -tea $test_after -ld $log_dir -mid $m -ps $procs_per_machine -ms $machines -is $iterations -gf $graph -ta $test_after -cf $config_file -ll $log_level -wsd $log_dir